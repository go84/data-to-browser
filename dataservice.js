var express = require('express');
var app = express();

var mongoose = require('mongoose');
var db = mongoose.connection;
mongoose.connect('mongodb://go84:go84@192.168.101.125/test');

db.on('error', console.error);
db.once('open', function() {
  console.log('connection open');
});
db.on('close', function() {
    console.log('disconnected');
});

var sniffSchema = new mongoose.Schema({timestamp: String, mac: String, rssi0: String, rssi1: String, sniffer: String});
var Sniff = mongoose.model('Sniff', sniffSchema);

var positionSchema = new mongoose.Schema({timestamp: Number, mac: String, x: Number, y: Number});
var Position = mongoose.model('Location', positionSchema);

app.get('/', function (req, res) {
  console.log('Request to /');


        Sniff.find({timestamp: {$lt: 1454702405.000000000}, timestamp: {$gt: 1454702400.000000000}},function(err, output) {
           if (err) return console.error(err);
           res.send(output);
        });

        // Sniff.find(function(err, output) { // 1454699242.241662000
        //    if (err) return console.error(err);
        //    res.send(output);
        // });

});

app.get('/one', (req, res) => {
  console.log('Request to /one');

  Sniff.findOne(function(err, output) {
     if (err) return console.error(err);
     res.send(output);
  });
});

app.param('mac', function (req, res, next, mac) {
  req.mac = mac;
  next();
});

app.get('/mac/:mac', (req, res) => {
  console.log('Request to /mac for mac: ', req.mac);

  Sniff.find({mac: req.mac}, (err, output) => {
     if (err) return console.error(err);
     res.send(output);
  });
});

app.param('sniffer', function (req, res, next, sniffer) {
  req.sniffer = sniffer;
  next();
});

app.get('/sniffer/:sniffer', (req, res) => {
  console.log('Request to /sniffer for sniffer: ', req.sniffer);

  Sniff.find({sniffer: req.sniffer}, (err, output) => {
     if (err) return console.error(err);
     res.send(output);
  });
});

// app.param('startTime', function (req, res, next, startTime) {
//   req.startTime = startTime;
//   next();
// });
//
// app.param('endTime', function (req, res, next, endTime) {
//   req.endTime = endTime;
//   next();
// });
//
// app.get('/timeframe/:startTime/:endTime', function (req, res) {
//   console.log('Request to /timeframe: ' + req.startTime + ' to ' + req.endTime);
//
//   Sniff.find({timestamp: {$lt: req.startTime}, timestamp: {$gt: req.endTime}},function(err, output) {
//      if (err) return console.error(err);
//      res.send(output);
//   });
// });

// Get by _id
app.param('id', function (req, res, next, id) {
  req.id = id;
  next();
});
app.param('limit', function (req, res, next, limit) {
  req.limit = limit;
  next();
});

app.get('/id/:id/:limit', function (req, res) {
  console.log('Request to /id: ' + req.id + ' limit ' + req.limit);

  Sniff.find({_id: {$gt: req.id}})
       .sort('_id')
       .limit(req.limit)
       .exec(function(err, output) {
         if (err) return console.error(err);
         res.json(output);
       });
});

// Position
// positionSchema = new mongoose.Schema({timestamp: Number, mac: String, x: Number, y: Number});

app.get('/position/mac/:mac', (req, res) => {
  console.log('Request to /position/mac for mac: ', req.mac);

  Position.find({mac: req.mac})
          //.sort('timestamp')
          .exec((err, output) => {
            if (err) return console.error(err);
            res.send(output);
          });
});

app.get('/position', (req, res) => {
  console.log('Request to /position');

  Position.find()
          //.sort('timestamp')
          .exec((err, output) => {
            if (err) return console.error(err);
            res.send(output);
          });
});



app.listen(3300, '0.0.0.0', function () {
    console.log('Example app listening on port 3300!');
});
